package com.liusongpo.util;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PoiDemo {

    public static void main(String[] args) {
        String filePath = new File("").getAbsolutePath() + File.separator + "src\\main\\resources\\public\\test.docx";
        String filePrefix = String.valueOf(Instant.now().getEpochSecond());
        String targetPath = new File("").getAbsolutePath() + File.separator + "test_" + filePrefix + ".docx";

        // 需要替换的变量
        Map<String, String> data = new HashMap<>();
        data.put("${masterName}", "珠海恩派非营利组织发展中心");
        data.put("${masterPerson}", "杨志成");
        data.put("${masterPhone}", "13726243732");
        data.put("${masterAddress}", "珠海市香洲区香溪路42号康兴大厦一楼恩派724社创空间");
        data.put("${masterSite}", "开放厨房和开放空间");

        data.put("${amount}", "400.00");
        data.put("${amountUpper}", "肆佰元整");

        data.put("${fromYear}", "2019");
        data.put("${fromMonth}", "03");
        data.put("${fromDay}", "08");
        data.put("${fromHour}", "16");
        data.put("${fromMinute}", "00");

        data.put("${toYear}", "2019");
        data.put("${toMonth}", "03");
        data.put("${toDay}", "08");
        data.put("${toHour}", "16");
        data.put("${toMinute}", "00");

        data.put("${totalHour}", "5");

        data.put("${actionName}", "尾牙活动");

        data.put("${cardName}", "珠海恩派非营利组织发展中心");
        data.put("${cardNumber}", "713360576133");
        data.put("${cardBank}", "中国银行珠海柠溪支行");

        data.put("${leaseName}", "测试组织名称");
        data.put("${leasePerson}", "测试联系人");
        data.put("${leasePhone}", "测试联系方式");
        data.put("${leaseAddress}", "测试联系地址");

        processDocxTemplate(filePath, targetPath, data);

    }

    /**
     * 渲染Docx模板
     * @param filePath 模板文件位置
     * @param targetPath 目标文件位置
     */
    private static void processDocxTemplate(String filePath, String targetPath, Map<String, String> data) {
        String tempPath = System.getProperty("java.io.tmpdir") + File.separator + "tmp_" + new File(filePath).getName();

        // 复制模板到临时文件，为啥复制呢？因为下一步写出文件的时候，poi会改动源文件
        try {
            FileUtils.copyFile(new File(filePath), new File(tempPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 为啥要再整个
        try (FileOutputStream fos = new FileOutputStream(targetPath); XWPFDocument doc = new XWPFDocument(OPCPackage.open(tempPath))) {
            doc.getParagraphs().forEach(e -> {
                StringBuilder builder = new StringBuilder();
                List<XWPFRun> list = e.getRuns();
                for (XWPFRun xwpfRun : list) {
                    builder.append(xwpfRun.getText(0));
                }

                // 当前读取到的文本内容
                String text = builder.toString();

                // 循环替换变量
                data.forEach((k, v) -> {
                    // 如果当前读入的文本内容中包含要替换的变量，则开始进行变量替换
                    if (text.contains(k)) {
                        String temp = text;

                        // 替换模板字符串
                        temp = temp.replace(k, v);

                        // 计算新字符在runs中的位置
                        // 如果字符串的长度不超过runs的长度，则从runs的开始开始放，后边的全部设置为空字符串
                        // 如果字符串的长度查过runs的长度，则根据ruans的长度开始计算放入每个run的字符串长度
                        if (temp.length() < list.size()) {
                            for (int i = 0; i < list.size(); i++) {
                                if (i < temp.length()) {
                                    list.get(i).setText(String.valueOf(temp.charAt(i)));
                                } else {
                                    list.get(i).setText("");
                                }
                            }
                        } else {
                            // 每次取的字符串长度
                            int count = temp.length() / list.size() + temp.length() % list.size();
                            // 可以按整批来取的次数，超过的是剩余的字符数
                            int times = temp.length() / count;
                            for (int i = 0; i < list.size(); i++) {
                                XWPFRun run = list.get(i);
                                // 如果超过最大整组获取数量，则取剩余的字符串
                                if (i < times) {
                                    run.setText(temp.substring(i * count, i * count + count), 0);
                                } else {
                                    run.setText(temp.substring(times * count), 0);
                                }
                            }
                        }
                    }
                });
            });

            doc.write(fos);
        } catch (IOException | InvalidFormatException e) {
            e.printStackTrace();
        }

        // 清理临时文件
        try {
            FileUtils.forceDelete(new File(tempPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
