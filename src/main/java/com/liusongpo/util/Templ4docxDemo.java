package com.liusongpo.util;

import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.core.VariablePattern;
import pl.jsolve.templ4docx.variable.TextVariable;
import pl.jsolve.templ4docx.variable.Variables;

import java.io.File;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class Templ4docxDemo {

    public static void main(String[] args) {
        String filePath = new File("").getAbsolutePath() + "\\src\\main\\resources\\public\\test1.docx";
        String filePrefix = String.valueOf(Instant.now().getEpochSecond());
        String targetPath = new File("").getAbsolutePath() + File.separator + "templ4docx_" + filePrefix + ".docx";

        // 需要替换的变量
        Map<String, String> data = new HashMap<>();
        data.put("masterName", "珠海恩派非营利组织发展中心");
        data.put("masterPerson", "杨志成");
        data.put("masterPhone", "13726243732");
        data.put("masterAddress", "珠海市香洲区香溪路42号康兴大厦一楼恩派724社创空间");
        data.put("masterSite", "开放厨房和开放空间");

        data.put("amount", "400.00");
        data.put("amountUpper", "肆佰元整");

        data.put("fromYear", "2019");
        data.put("fromMonth", "03");
        data.put("fromDay", "08");
        data.put("fromHour", "16");

        data.put("toYear", "2019");
        data.put("toMonth", "03");
        data.put("toDay", "08");
        data.put("toHour", "16");

        data.put("totalHour", "5");

        data.put("actionName", "尾牙活动");

        data.put("cardName", "珠海恩派非营利组织发展中心");
        data.put("cardNumber", "713360576133");
        data.put("cardBank", "中国银行珠海柠溪支行");

        data.put("leaseName", "测试组织名称");
        data.put("leasePerson", "测试联系人");
        data.put("leasePhone", "测试联系方式");
        data.put("leaseAddress", "测试联系地址");

        String prefix = "${";
        String suffix = "}";

        render(filePath, targetPath, data, prefix, suffix);
    }

    /**
     * 填充docx模板
     *
     * @param filePath   模板路径 E:\test.docx
     * @param targetPath 目标文件路径 E:\target.docx
     * @param data       模板数据 模板数据map集合，key例如：username，不能被变量前缀和后缀包含
     * @param prefix     变量前缀 例如：${
     * @param suffix     变量后缀 例如：}
     */
    private static void render(String filePath, String targetPath, Map<String, String> data, String prefix, String suffix) {
        // 初始化
        Docx docx = new Docx(filePath);

        // 设置变量占位符前缀和后缀
        docx.setVariablePattern(new VariablePattern(prefix, suffix));

        // 进行变量与变量值配对
        Variables variables = new Variables();
        data.forEach((k, v) -> variables.addTextVariable(new TextVariable(prefix + k + suffix, v)));

        // 填充模板
        docx.fillTemplate(variables);

        // 写出文件
        docx.save(targetPath);
    }


}
