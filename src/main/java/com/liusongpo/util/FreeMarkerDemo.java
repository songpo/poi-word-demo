package com.liusongpo.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class FreeMarkerDemo {

    public static void main(String[] args) {
        String filePath = new File("").getAbsolutePath() + File.separator + "src\\main\\resources\\templates\\contract.ftl";

        // 需要替换的变量
        Map<String, String> data = new HashMap<>();
        data.put("masterName", "珠海恩派非营利组织发展中心");
        data.put("masterPerson", "杨志成");
        data.put("masterPhone", "13726243732");
        data.put("masterAddress", "珠海市香洲区香溪路42号康兴大厦一楼恩派724社创空间");
        data.put("masterSite", "开放厨房和开放空间");

        data.put("amount", "400.00");
        data.put("amountUpper", "肆佰元整");

        data.put("fromYear", "2019");
        data.put("fromMonth", "03");
        data.put("fromDay", "08");
        data.put("fromHour", "16");

        data.put("toYear", "2019");
        data.put("toMonth", "03");
        data.put("toDay", "08");
        data.put("toHour", "16");

        data.put("totalHour", "5");

        data.put("actionName", "尾牙活动");

        data.put("cardName", "珠海恩派非营利组织发展中心");
        data.put("cardNumber", "713360576133");
        data.put("cardBank", "中国银行珠海柠溪支行");

        data.put("leaseName", "测试组织名称");
        data.put("leasePerson", "测试联系人");
        data.put("leasePhone", "测试联系方式");
        data.put("leaseAddress", "测试联系地址");


        String tempFileName = "tmp_" + new File(filePath).getName();
        String tempPath = System.getProperty("java.io.tmpdir") + File.separator + tempFileName;

        try {
            FileUtils.forceDelete(new File(tempPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("tempPath = " + tempPath);

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
        configuration.setDefaultEncoding("UTF-8");

        String filePrefix = String.valueOf(Instant.now().getEpochSecond());
        String targetPath = new File("").getAbsolutePath() + File.separator + "test_" + filePrefix + ".docx";
        try (FileWriter fileWriter = new FileWriter(new File(targetPath))) {

            configuration.setDirectoryForTemplateLoading(new File(tempPath).getParentFile());

            // 复制模板到临时文件，为啥复制呢？因为下一步写出文件的时候，poi会改动源文件
            try {
                FileUtils.copyFile(new File(filePath), new File(tempPath));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Template template = configuration.getTemplate(tempFileName);
            template.process(data, fileWriter);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }
}
